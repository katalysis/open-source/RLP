import XCTest
@testable import RLP

// TODO: implement the following tests: https://github.com/ethereum/go-ethereum/blob/master/rlp/encode_test.go

class RLPTests: XCTestCase {
    func testEncodeShortString() throws {
        let str = "dog"
        let enc = try RLPEncoder.encode(str)
        let res = [0x83] + [UInt8](str.utf8)
        XCTAssertEqual(enc, res)
    }

    func testEncodeLongString() throws {
        let str = "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
        let enc = try RLPEncoder.encode(str)
        let res = [0xb8, 0x38] + [UInt8](str.utf8)
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeEmptyString() throws {
        let enc = try RLPEncoder.encode("")
        let res: [UInt8] = [0x80]
        XCTAssertEqual(enc, res)
    }
    
    
    func testEncodeNil() throws {
        let enc = try RLPEncoder.encode(nil)
        let res: [UInt8] = [0x80]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeZero() throws {
        let enc = try RLPEncoder.encode(0)
        let res: [UInt8] = [0x80]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeUInt8() throws {
        let enc = try RLPEncoder.encode(UInt64(15))
        let res: [UInt8] = [0x0F]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeUInt64() throws {
        let enc = try RLPEncoder.encode(1024)
        let res: [UInt8] = [0x82, 0x04, 0x00]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodePositiveInt() throws {
        let enc = try RLPEncoder.encode(Int(1024))
        let res: [UInt8] = [0x82, 0x04, 0x00]
        XCTAssertEqual(enc, res)
        
    }
    
    func testEncodeNegativeNumber() {
        XCTAssertThrowsError(try RLPEncoder.encode(-1))
    }
    

    func testEncodeArray() throws {
        let enc = try RLPEncoder.encode(["cat", "dog"])
        let res: [UInt8] = [0xc8, 0x83] + [UInt8]("cat".utf8) + [0x83] + [UInt8]("dog".utf8)
        XCTAssertEqual(enc, res)
    }
    
    
    func testEncodeEmptyArray() throws {
        let enc = try RLPEncoder.encode([])
        let res: [UInt8] = [0xC0]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeNestedArrays() throws {
        let enc = try RLPEncoder.encode([ [], [[]], [ [], [[]] ] ])
        let res: [UInt8] = [ 0xc7, 0xc0, 0xc1, 0xc0, 0xc3, 0xc0, 0xc1, 0xc0 ]
        XCTAssertEqual(enc, res)
    }
    
    func testEncodeDict() {
        XCTAssertThrowsError(try RLPEncoder.encode([1:2]))
    }
    
    
    func testEncodeComplex() throws {
        
        let a0: [UInt8] = [0x28,0xEF,0x61,0x34,0x0B,0xD9,0x39,0xBC,0x21,0x95,0xFE,0x53,0x75,0x67,0x86,0x60,0x03,0xE1,0xA1,0x5D,0x3C,0x71,0xFF,0x63,0xE1,0x59,0x06,0x20,0xAA,0x63,0x62,0x76]
        let a1: [UInt8] = [0x67,0xCB,0xE9,0xD8,0x99,0x7F,0x76,0x1A,0xEC,0xB7,0x03,0x30,0x4B,0x38,0x00,0xCC,0xF5,0x55,0xC9,0xF3,0xDC,0x64,0x21,0x4B,0x29,0x7F,0xB1,0x96,0x6A,0x3B,0x6D,0x83]
        
        let enc = try RLPEncoder.encode(a0)
        print(enc.reduce("", { (r, u) -> String in
            return r + String(format: "%02X", u)}))
    }

    
    func testEncodeComplex2() throws {
        let a0: [UInt8] = [0x28,0xEF,0x61,0x34,0x0B,0xD9,0x39,0xBC,0x21,0x95,0xFE,0x53,0x75,0x67,0x86,0x60,0x03,0xE1,0xA1,0x5D,0x3C,0x71,0xFF,0x63,0xE1,0x59,0x06,0x20,0xAA,0x63,0x62,0x76]
        let a1: [UInt8] = [0x67,0xCB,0xE9,0xD8,0x99,0x7F,0x76,0x1A,0xEC,0xB7,0x03,0x30,0x4B,0x38,0x00,0xCC,0xF5,0x55,0xC9,0xF3,0xDC,0x64,0x21,0x4B,0x29,0x7F,0xB1,0x96,0x6A,0x3B,0x6D,0x83]
        
        let enc = try RLPEncoder.encode([9, 20000000000, 21000, "55555555555555555555", 1000000000000000000, "", 0, a0, a1])
        print(enc.reduce("", { (r, u) -> String in
            return r + String(format: "%02X", u)}))
    }
    
    func testEncodeVeryLongString() throws {
    let str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur mauris magna, suscipit sed vehicula non, iaculis faucibus tortor. Proin suscipit ultricies malesuada. Duis tortor elit, dictum quis tristique eu, ultrices at risus. Morbi a est imperdiet mi ullamcorper aliquet suscipit nec lorem. Aenean quis leo mollis, vulputate elit varius, consequat enim. Nulla ultrices turpis justo, et posuere urna consectetur nec. Proin non convallis metus. Donec tempor ipsum in mauris congue sollicitudin. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse convallis sem vel massa faucibus, eget lacinia lacus tempor. Nulla quis ultricies purus. Proin auctor rhoncus nibh condimentum mollis. Aliquam consequat enim at metus luctus, a eleifend purus egestas. Curabitur at nibh metus. Nam bibendum, neque at auctor tristique, lorem libero aliquet arcu, non interdum tellus lectus sit amet eros. Cras rhoncus, metus ac ornare cursus, dolor justo ultrices metus, at ullamcorper volutpat"
    let res = "B904004C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E73656374657475722061646970697363696E6720656C69742E20437572616269747572206D6175726973206D61676E612C20737573636970697420736564207665686963756C61206E6F6E2C20696163756C697320666175636962757320746F72746F722E2050726F696E20737573636970697420756C74726963696573206D616C6573756164612E204475697320746F72746F7220656C69742C2064696374756D2071756973207472697374697175652065752C20756C7472696365732061742072697375732E204D6F72626920612065737420696D70657264696574206D6920756C6C616D636F7270657220616C6971756574207375736369706974206E6563206C6F72656D2E2041656E65616E2071756973206C656F206D6F6C6C69732C2076756C70757461746520656C6974207661726975732C20636F6E73657175617420656E696D2E204E756C6C6120756C74726963657320747572706973206A7573746F2C20657420706F73756572652075726E6120636F6E7365637465747572206E65632E2050726F696E206E6F6E20636F6E76616C6C6973206D657475732E20446F6E65632074656D706F7220697073756D20696E206D617572697320636F6E67756520736F6C6C696369747564696E2E20566573746962756C756D20616E746520697073756D207072696D697320696E206661756369627573206F726369206C756374757320657420756C74726963657320706F737565726520637562696C69612043757261653B2053757370656E646973736520636F6E76616C6C69732073656D2076656C206D617373612066617563696275732C2065676574206C6163696E6961206C616375732074656D706F722E204E756C6C61207175697320756C747269636965732070757275732E2050726F696E20617563746F722072686F6E637573206E69626820636F6E64696D656E74756D206D6F6C6C69732E20416C697175616D20636F6E73657175617420656E696D206174206D65747573206C75637475732C206120656C656966656E6420707572757320656765737461732E20437572616269747572206174206E696268206D657475732E204E616D20626962656E64756D2C206E6571756520617420617563746F72207472697374697175652C206C6F72656D206C696265726F20616C697175657420617263752C206E6F6E20696E74657264756D2074656C6C7573206C65637475732073697420616D65742065726F732E20437261732072686F6E6375732C206D65747573206163206F726E617265206375727375732C20646F6C6F72206A7573746F20756C747269636573206D657475732C20617420756C6C616D636F7270657220766F6C7574706174"
        let enc = try RLPEncoder.encode(str)
        let resStr = enc.reduce("") { (r, u) -> String in
            return r + String(format: "%02X", u)
        }
        XCTAssertEqual(resStr, res)
    
    }
    
    func testData() throws {
    let a0: [UInt8] = [0x21, 0x6C, 0x57, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x68, 0x74, 0x74, 0x70, 0x3A, 0x2F, 0x2F, 0x39, 0x35, 0x2E, 0x38, 0x35, 0x2E, 0x32, 0x36, 0x2E, 0x39, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12, 0x68, 0x74, 0x74, 0x70, 0x3A, 0x2F, 0x2F, 0x39, 0x35, 0x2E, 0x38, 0x35, 0x2E, 0x32, 0x36, 0x2E, 0x39, 0x39, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

        let res = "B8C4216C5741000000000000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000000800000000000000000000000000000000000000000000000000000000000000012687474703A2F2F39352E38352E32362E393900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012687474703A2F2F39352E38352E32362E39390000000000000000000000000000"
        let enc = try RLPEncoder.encode(a0)
        let resStr = enc.reduce("") { (r, u) -> String in
            return r + String(format: "%02X", u)
        }
        XCTAssertEqual(resStr, res)
    }

    
    func testLeadingZeroRemoval() throws {
        let leadingZero = 1
        let a0: [UInt8] = [UInt8](repeating: 0, count:leadingZero) + [UInt8](repeating: 1, count: 4 - leadingZero)
        
        let res = "83010101"
        let enc = try RLPEncoder.encode(a0)
        let resStr = enc.reduce("") { (r, u) -> String in
            return r + String(format: "%02X", u)
        }
        XCTAssertEqual(resStr, res)
    }
    
    static var allTests = [
        ("testEncodeShortString", testEncodeShortString),
        ("testEncodeLongString", testEncodeLongString),
        ("testEncodeEmptyString", testEncodeEmptyString),
        ("testEncodeNil", testEncodeNil),
        ("testEncodeZero", testEncodeZero),
        ("testEncodeUInt8", testEncodeUInt8),
        ("testEncodeUInt64", testEncodeUInt64),
        ("testEncodePositiveInt", testEncodePositiveInt),
        ("testEncodeNegativeNumber", testEncodeNegativeNumber),
        ("testEncodeArray", testEncodeArray),
        ("testEncodeEmptyArray", testEncodeEmptyArray),
        ("testEncodeNestedArrays", testEncodeNestedArrays),
        ("testEncodeDict", testEncodeDict),
        ("testEncodeComplex", testEncodeComplex),
        ("testEncodeVeryLongString", testEncodeVeryLongString),
        ("testData", testData),
        ("testLeadingZeroRemoval", testLeadingZeroRemoval),
    ]
}

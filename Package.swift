// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "RLP",
    products: [ 
     .library(name: "RLP", targets: ["RLP"])
    ],
    targets: [
       .target(name: "RLP", path: ".", sources: ["Sources"]),
    ]
)
